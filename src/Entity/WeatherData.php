<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WeatherData
 *
 * @ORM\Table(name="weather_data")
 * @ORM\Entity(repositoryClass="App\Repository\WeatherDataRepository")
 */
class WeatherData
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numer_sta", type="string", length=255, nullable=true)
     */
    private $numerSta;

    /**
     * @var string|null
     *
     * @ORM\Column(name="date", type="string", length=255, nullable=true)
     */
    private $date;

    /**
     * @var int|null
     *
     * @ORM\Column(name="pmer", type="integer", nullable=true)
     */
    private $pmer;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tend", type="integer", nullable=true)
     */
    private $tend;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cod_tend", type="integer", nullable=true)
     */
    private $codTend;

    /**
     * @var int|null
     *
     * @ORM\Column(name="dd", type="integer", nullable=true)
     */
    private $dd;

    /**
     * @var float|null
     *
     * @ORM\Column(name="ff", type="float", precision=10, scale=0, nullable=true)
     */
    private $ff;

    /**
     * @var float|null
     *
     * @ORM\Column(name="t", type="float", precision=10, scale=0, nullable=true)
     */
    private $t;

    /**
     * @var float|null
     *
     * @ORM\Column(name="td", type="float", precision=10, scale=0, nullable=true)
     */
    private $td;

    /**
     * @var int|null
     *
     * @ORM\Column(name="u", type="integer", nullable=true)
     */
    private $u;

    /**
     * @var float|null
     *
     * @ORM\Column(name="vv", type="float", precision=10, scale=0, nullable=true)
     */
    private $vv;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ww", type="integer", nullable=true)
     */
    private $ww;

    /**
     * @var int|null
     *
     * @ORM\Column(name="w1", type="integer", nullable=true)
     */
    private $w1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="w2", type="integer", nullable=true)
     */
    private $w2;

    /**
     * @var float|null
     *
     * @ORM\Column(name="n", type="float", precision=10, scale=0, nullable=true)
     */
    private $n;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nbas", type="integer", nullable=true)
     */
    private $nbas;

    /**
     * @var int|null
     *
     * @ORM\Column(name="hbas", type="integer", nullable=true)
     */
    private $hbas;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cl", type="integer", nullable=true)
     */
    private $cl;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cm", type="integer", nullable=true)
     */
    private $cm;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ch", type="integer", nullable=true)
     */
    private $ch;

    /**
     * @var int|null
     *
     * @ORM\Column(name="pres", type="integer", nullable=true)
     */
    private $pres;

    /**
     * @var int|null
     *
     * @ORM\Column(name="niv_bar", type="integer", nullable=true)
     */
    private $nivBar;

    /**
     * @var int|null
     *
     * @ORM\Column(name="geop", type="integer", nullable=true)
     */
    private $geop;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tend24", type="integer", nullable=true)
     */
    private $tend24;

    /**
     * @var float|null
     *
     * @ORM\Column(name="tminsol", type="float", precision=10, scale=0, nullable=true)
     */
    private $tminsol;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sw", type="integer", nullable=true)
     */
    private $sw;

    /**
     * @var float|null
     *
     * @ORM\Column(name="tw", type="float", precision=10, scale=0, nullable=true)
     */
    private $tw;

    /**
     * @var float|null
     *
     * @ORM\Column(name="raf10", type="float", precision=10, scale=0, nullable=true)
     */
    private $raf10;

    /**
     * @var float|null
     *
     * @ORM\Column(name="rafper", type="float", precision=10, scale=0, nullable=true)
     */
    private $rafper;

    /**
     * @var float|null
     *
     * @ORM\Column(name="per", type="float", precision=10, scale=0, nullable=true)
     */
    private $per;

    /**
     * @var int|null
     *
     * @ORM\Column(name="etat_sol", type="integer", nullable=true)
     */
    private $etatSol;

    /**
     * @var float|null
     *
     * @ORM\Column(name="ht_neige", type="float", precision=10, scale=0, nullable=true)
     */
    private $htNeige;

    /**
     * @var float|null
     *
     * @ORM\Column(name="ssfrai", type="float", precision=10, scale=0, nullable=true)
     */
    private $ssfrai;

    /**
     * @var float|null
     *
     * @ORM\Column(name="perssfrai", type="float", precision=10, scale=0, nullable=true)
     */
    private $perssfrai;

    /**
     * @var float|null
     *
     * @ORM\Column(name="tn12", type="float", precision=10, scale=0, nullable=true)
     */
    private $tn12;

    /**
     * @var float|null
     *
     * @ORM\Column(name="tn24", type="float", precision=10, scale=0, nullable=true)
     */
    private $tn24;

    /**
     * @var float|null
     *
     * @ORM\Column(name="tx12", type="float", precision=10, scale=0, nullable=true)
     */
    private $tx12;

    /**
     * @var float|null
     *
     * @ORM\Column(name="tx24", type="float", precision=10, scale=0, nullable=true)
     */
    private $tx24;

    /**
     * @var float|null
     *
     * @ORM\Column(name="rr1", type="float", precision=10, scale=0, nullable=true)
     */
    private $rr1;

    /**
     * @var float|null
     *
     * @ORM\Column(name="rr3", type="float", precision=10, scale=0, nullable=true)
     */
    private $rr3;

    /**
     * @var float|null
     *
     * @ORM\Column(name="rr6", type="float", precision=10, scale=0, nullable=true)
     */
    private $rr6;

    /**
     * @var float|null
     *
     * @ORM\Column(name="rr12", type="float", precision=10, scale=0, nullable=true)
     */
    private $rr12;

    /**
     * @var float|null
     *
     * @ORM\Column(name="rr24", type="float", precision=10, scale=0, nullable=true)
     */
    private $rr24;

    /**
     * @var int|null
     *
     * @ORM\Column(name="phenspe1", type="integer", nullable=true)
     */
    private $phenspe1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="phenspe2", type="integer", nullable=true)
     */
    private $phenspe2;

    /**
     * @var int|null
     *
     * @ORM\Column(name="phenspe3", type="integer", nullable=true)
     */
    private $phenspe3;

    /**
     * @var int|null
     *
     * @ORM\Column(name="phenspe4", type="integer", nullable=true)
     */
    private $phenspe4;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nnuage1", type="integer", nullable=true)
     */
    private $nnuage1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ctype1", type="integer", nullable=true)
     */
    private $ctype1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="hnuage1", type="integer", nullable=true)
     */
    private $hnuage1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nnuage2", type="integer", nullable=true)
     */
    private $nnuage2;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ctype2", type="integer", nullable=true)
     */
    private $ctype2;

    /**
     * @var int|null
     *
     * @ORM\Column(name="hnuage2", type="integer", nullable=true)
     */
    private $hnuage2;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nnuage3", type="integer", nullable=true)
     */
    private $nnuage3;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ctype3", type="integer", nullable=true)
     */
    private $ctype3;

    /**
     * @var int|null
     *
     * @ORM\Column(name="hnuage3", type="integer", nullable=true)
     */
    private $hnuage3;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nnuage4", type="integer", nullable=true)
     */
    private $nnuage4;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ctype4", type="integer", nullable=true)
     */
    private $ctype4;

    /**
     * @var int|null
     *
     * @ORM\Column(name="hnuage4", type="integer", nullable=true)
     */
    private $hnuage4;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumerSta(): ?string
    {
        return $this->numerSta;
    }

    public function setNumerSta(?string $numerSta): self
    {
        $this->numerSta = $numerSta;

        return $this;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(?string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getPmer(): ?int
    {
        return $this->pmer;
    }

    public function setPmer(?int $pmer): self
    {
        $this->pmer = $pmer;

        return $this;
    }

    public function getTend(): ?int
    {
        return $this->tend;
    }

    public function setTend(?int $tend): self
    {
        $this->tend = $tend;

        return $this;
    }

    public function getCodTend(): ?int
    {
        return $this->codTend;
    }

    public function setCodTend(?int $codTend): self
    {
        $this->codTend = $codTend;

        return $this;
    }

    public function getDd(): ?int
    {
        return $this->dd;
    }

    public function setDd(?int $dd): self
    {
        $this->dd = $dd;

        return $this;
    }

    public function getFf(): ?float
    {
        return $this->ff;
    }

    public function setFf(?float $ff): self
    {
        $this->ff = $ff;

        return $this;
    }

    public function getT(): ?float
    {
        return $this->t;
    }

    public function setT(?float $t): self
    {
        $this->t = $t;

        return $this;
    }

    public function getTd(): ?float
    {
        return $this->td;
    }

    public function setTd(?float $td): self
    {
        $this->td = $td;

        return $this;
    }

    public function getU(): ?int
    {
        return $this->u;
    }

    public function setU(?int $u): self
    {
        $this->u = $u;

        return $this;
    }

    public function getVv(): ?float
    {
        return $this->vv;
    }

    public function setVv(?float $vv): self
    {
        $this->vv = $vv;

        return $this;
    }

    public function getWw(): ?int
    {
        return $this->ww;
    }

    public function setWw(?int $ww): self
    {
        $this->ww = $ww;

        return $this;
    }

    public function getW1(): ?int
    {
        return $this->w1;
    }

    public function setW1(?int $w1): self
    {
        $this->w1 = $w1;

        return $this;
    }

    public function getW2(): ?int
    {
        return $this->w2;
    }

    public function setW2(?int $w2): self
    {
        $this->w2 = $w2;

        return $this;
    }

    public function getN(): ?float
    {
        return $this->n;
    }

    public function setN(?float $n): self
    {
        $this->n = $n;

        return $this;
    }

    public function getNbas(): ?int
    {
        return $this->nbas;
    }

    public function setNbas(?int $nbas): self
    {
        $this->nbas = $nbas;

        return $this;
    }

    public function getHbas(): ?int
    {
        return $this->hbas;
    }

    public function setHbas(?int $hbas): self
    {
        $this->hbas = $hbas;

        return $this;
    }

    public function getCl(): ?int
    {
        return $this->cl;
    }

    public function setCl(?int $cl): self
    {
        $this->cl = $cl;

        return $this;
    }

    public function getCm(): ?int
    {
        return $this->cm;
    }

    public function setCm(?int $cm): self
    {
        $this->cm = $cm;

        return $this;
    }

    public function getCh(): ?int
    {
        return $this->ch;
    }

    public function setCh(?int $ch): self
    {
        $this->ch = $ch;

        return $this;
    }

    public function getPres(): ?int
    {
        return $this->pres;
    }

    public function setPres(?int $pres): self
    {
        $this->pres = $pres;

        return $this;
    }

    public function getNivBar(): ?int
    {
        return $this->nivBar;
    }

    public function setNivBar(?int $nivBar): self
    {
        $this->nivBar = $nivBar;

        return $this;
    }

    public function getGeop(): ?int
    {
        return $this->geop;
    }

    public function setGeop(?int $geop): self
    {
        $this->geop = $geop;

        return $this;
    }

    public function getTend24(): ?int
    {
        return $this->tend24;
    }

    public function setTend24(?int $tend24): self
    {
        $this->tend24 = $tend24;

        return $this;
    }

    public function getTminsol(): ?float
    {
        return $this->tminsol;
    }

    public function setTminsol(?float $tminsol): self
    {
        $this->tminsol = $tminsol;

        return $this;
    }

    public function getSw(): ?int
    {
        return $this->sw;
    }

    public function setSw(?int $sw): self
    {
        $this->sw = $sw;

        return $this;
    }

    public function getTw(): ?float
    {
        return $this->tw;
    }

    public function setTw(?float $tw): self
    {
        $this->tw = $tw;

        return $this;
    }

    public function getRaf10(): ?float
    {
        return $this->raf10;
    }

    public function setRaf10(?float $raf10): self
    {
        $this->raf10 = $raf10;

        return $this;
    }

    public function getRafper(): ?float
    {
        return $this->rafper;
    }

    public function setRafper(?float $rafper): self
    {
        $this->rafper = $rafper;

        return $this;
    }

    public function getPer(): ?float
    {
        return $this->per;
    }

    public function setPer(?float $per): self
    {
        $this->per = $per;

        return $this;
    }

    public function getEtatSol(): ?int
    {
        return $this->etatSol;
    }

    public function setEtatSol(?int $etatSol): self
    {
        $this->etatSol = $etatSol;

        return $this;
    }

    public function getHtNeige(): ?float
    {
        return $this->htNeige;
    }

    public function setHtNeige(?float $htNeige): self
    {
        $this->htNeige = $htNeige;

        return $this;
    }

    public function getSsfrai(): ?float
    {
        return $this->ssfrai;
    }

    public function setSsfrai(?float $ssfrai): self
    {
        $this->ssfrai = $ssfrai;

        return $this;
    }

    public function getPerssfrai(): ?float
    {
        return $this->perssfrai;
    }

    public function setPerssfrai(?float $perssfrai): self
    {
        $this->perssfrai = $perssfrai;

        return $this;
    }

    public function getTn12(): ?float
    {
        return $this->tn12;
    }

    public function setTn12(?float $tn12): self
    {
        $this->tn12 = $tn12;

        return $this;
    }

    public function getTn24(): ?float
    {
        return $this->tn24;
    }

    public function setTn24(?float $tn24): self
    {
        $this->tn24 = $tn24;

        return $this;
    }

    public function getTx12(): ?float
    {
        return $this->tx12;
    }

    public function setTx12(?float $tx12): self
    {
        $this->tx12 = $tx12;

        return $this;
    }

    public function getTx24(): ?float
    {
        return $this->tx24;
    }

    public function setTx24(?float $tx24): self
    {
        $this->tx24 = $tx24;

        return $this;
    }

    public function getRr1(): ?float
    {
        return $this->rr1;
    }

    public function setRr1(?float $rr1): self
    {
        $this->rr1 = $rr1;

        return $this;
    }

    public function getRr3(): ?float
    {
        return $this->rr3;
    }

    public function setRr3(?float $rr3): self
    {
        $this->rr3 = $rr3;

        return $this;
    }

    public function getRr6(): ?float
    {
        return $this->rr6;
    }

    public function setRr6(?float $rr6): self
    {
        $this->rr6 = $rr6;

        return $this;
    }

    public function getRr12(): ?float
    {
        return $this->rr12;
    }

    public function setRr12(?float $rr12): self
    {
        $this->rr12 = $rr12;

        return $this;
    }

    public function getRr24(): ?float
    {
        return $this->rr24;
    }

    public function setRr24(?float $rr24): self
    {
        $this->rr24 = $rr24;

        return $this;
    }

    public function getPhenspe1(): ?int
    {
        return $this->phenspe1;
    }

    public function setPhenspe1(?int $phenspe1): self
    {
        $this->phenspe1 = $phenspe1;

        return $this;
    }

    public function getPhenspe2(): ?int
    {
        return $this->phenspe2;
    }

    public function setPhenspe2(?int $phenspe2): self
    {
        $this->phenspe2 = $phenspe2;

        return $this;
    }

    public function getPhenspe3(): ?int
    {
        return $this->phenspe3;
    }

    public function setPhenspe3(?int $phenspe3): self
    {
        $this->phenspe3 = $phenspe3;

        return $this;
    }

    public function getPhenspe4(): ?int
    {
        return $this->phenspe4;
    }

    public function setPhenspe4(?int $phenspe4): self
    {
        $this->phenspe4 = $phenspe4;

        return $this;
    }

    public function getNnuage1(): ?int
    {
        return $this->nnuage1;
    }

    public function setNnuage1(?int $nnuage1): self
    {
        $this->nnuage1 = $nnuage1;

        return $this;
    }

    public function getCtype1(): ?int
    {
        return $this->ctype1;
    }

    public function setCtype1(?int $ctype1): self
    {
        $this->ctype1 = $ctype1;

        return $this;
    }

    public function getHnuage1(): ?int
    {
        return $this->hnuage1;
    }

    public function setHnuage1(?int $hnuage1): self
    {
        $this->hnuage1 = $hnuage1;

        return $this;
    }

    public function getNnuage2(): ?int
    {
        return $this->nnuage2;
    }

    public function setNnuage2(?int $nnuage2): self
    {
        $this->nnuage2 = $nnuage2;

        return $this;
    }

    public function getCtype2(): ?int
    {
        return $this->ctype2;
    }

    public function setCtype2(?int $ctype2): self
    {
        $this->ctype2 = $ctype2;

        return $this;
    }

    public function getHnuage2(): ?int
    {
        return $this->hnuage2;
    }

    public function setHnuage2(?int $hnuage2): self
    {
        $this->hnuage2 = $hnuage2;

        return $this;
    }

    public function getNnuage3(): ?int
    {
        return $this->nnuage3;
    }

    public function setNnuage3(?int $nnuage3): self
    {
        $this->nnuage3 = $nnuage3;

        return $this;
    }

    public function getCtype3(): ?int
    {
        return $this->ctype3;
    }

    public function setCtype3(?int $ctype3): self
    {
        $this->ctype3 = $ctype3;

        return $this;
    }

    public function getHnuage3(): ?int
    {
        return $this->hnuage3;
    }

    public function setHnuage3(?int $hnuage3): self
    {
        $this->hnuage3 = $hnuage3;

        return $this;
    }

    public function getNnuage4(): ?int
    {
        return $this->nnuage4;
    }

    public function setNnuage4(?int $nnuage4): self
    {
        $this->nnuage4 = $nnuage4;

        return $this;
    }

    public function getCtype4(): ?int
    {
        return $this->ctype4;
    }

    public function setCtype4(?int $ctype4): self
    {
        $this->ctype4 = $ctype4;

        return $this;
    }

    public function getHnuage4(): ?int
    {
        return $this->hnuage4;
    }

    public function setHnuage4(?int $hnuage4): self
    {
        $this->hnuage4 = $hnuage4;

        return $this;
    }


}
