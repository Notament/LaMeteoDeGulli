<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StationRepository")
 */
class Station
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $numer_sta;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    public function __construct()
    {
        $this->weatherData = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNumerSta(): ?int
    {
        return $this->numer_sta;
    }

    public function setNumerSta(int $numer_sta): self
    {
        $this->numer_sta = $numer_sta;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|WeatherData[]
     */
    public function getWeatherData(): Collection
    {
        return $this->weatherData;
    }

    public function addWeatherData(WeatherData $weatherData): self
    {
        if (!$this->weatherData->contains($weatherData)) {
            $this->weatherData[] = $weatherData;
            $weatherData->setNumerSta($this);
        }

        return $this;
    }

    public function removeWeatherData(WeatherData $weatherData): self
    {
        if ($this->weatherData->contains($weatherData)) {
            $this->weatherData->removeElement($weatherData);
            // set the owning side to null (unless already changed)
            if ($weatherData->getNumerSta() === $this) {
                $weatherData->setNumerSta(null);
            }
        }

        return $this;
    }
}
