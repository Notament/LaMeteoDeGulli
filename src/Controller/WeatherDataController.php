<?php

namespace App\Controller;

use App\Entity\WeatherData;
use App\Form\WeatherDataType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/weather/data")
 */
class WeatherDataController extends Controller
{
    /**
     * @Route("/", name="weather_data_index", methods="GET")
     */
    public function index(): Response
    {
        $weatherDatas = $this->getDoctrine()
            ->getRepository(WeatherData::class)
            ->findAll();

        return $this->render('weather_data/index.html.twig', ['weather_datas' => $weatherDatas]);
    }

    /**
     * @Route("/new", name="weather_data_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $weatherDatum = new WeatherData();
        $form = $this->createForm(WeatherDataType::class, $weatherDatum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($weatherDatum);
            $em->flush();

            return $this->redirectToRoute('weather_data_index');
        }

        return $this->render('weather_data/new.html.twig', [
            'weather_datum' => $weatherDatum,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="weather_data_show", methods="GET")
     */
    public function show(WeatherData $weatherDatum): Response
    {
        return $this->render('weather_data/show.html.twig', ['weather_datum' => $weatherDatum]);
    }

    /**
     * @Route("/{id}/edit", name="weather_data_edit", methods="GET|POST")
     */
    public function edit(Request $request, WeatherData $weatherDatum): Response
    {
        $form = $this->createForm(WeatherDataType::class, $weatherDatum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('weather_data_edit', ['id' => $weatherDatum->getId()]);
        }

        return $this->render('weather_data/edit.html.twig', [
            'weather_datum' => $weatherDatum,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="weather_data_delete", methods="DELETE")
     */
    public function delete(Request $request, WeatherData $weatherDatum): Response
    {
        if ($this->isCsrfTokenValid('delete'.$weatherDatum->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($weatherDatum);
            $em->flush();
        }

        return $this->redirectToRoute('weather_data_index');
    }
}
