<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\WeatherData;
use App\Repository\WeatherDataRepository;

class MainController extends Controller
{

    private $stations = array(
        "7005" => "ABBEVILLE",
        "7015" => "LILLE-LESQUIN",
        "7020" => "PTE DE LA HAGUE",
        "7027" => "CAEN-CARPIQUET",
        "7037" => "ROUEN-BOOS",
        "7072" => "REIMS-PRUNAY",
        "7110" => "BREST-GUIPAVAS",
        "7117" => "PLOUMANAC'H",
        "7130" => "RENNES-ST JACQUES",
        "7139" => "ALENCON",
        "7149" => "ORLY",
        "7168" => "TROYES-BARBEREY",
        "7181" => "NANCY-OCHEY",
        "7190" => "STRASBOURG-ENTZHEIM",
        "7207" => "BELLE ILE-LE TALUT",
        "7222" => "NANTES-BOUGUENAIS",
        "7240" => "TOURS",
        "7255" => "BOURGES",
        "7280" => "DIJON-LONGVIC",
        "7299" => "BALE-MULHOUSE",
        "7314" => "PTE DE CHASSIRON",
        "7335" => "POITIERS-BIARD",
        "7434" => "LIMOGES-BELLEGARDE",
        "7460" => "CLERMONT-FD",
        "7471" => "LE PUY-LOUDES",
        "7481" => "LYON-ST EXUPERY",
        "7510" => "BORDEAUX-MERIGNAC",
        "7535" => "GOURDON",
        "7558" => "MILLAU",
        "7577" => "MONTELIMAR",
        "7591" => "EMBRUN",
        "7607" => "MONT-DE-MARSAN",
        "7621" => "TARBES-OSSUN",
        "7627" => "ST GIRONS",
        "7630" => "TOULOUSE-BLAGNAC",
        "7643" => "MONTPELLIER",
        "7650" => "MARIGNANE",
        "7661" => "CAP CEPET",
        "7690" => "NICE",
        "7747" => "PERPIGNAN",
        "7761" => "AJACCIO",
        "7790" => "BASTIA",
        "61968" => "GLORIEUSES",
        "61970" => "JUAN DE NOVA",
        "61972" => "EUROPA",
        "61976" => "TROMELIN",
        "61980" => "GILLOT-AEROPORT",
        "61996" => "NOUVELLE AMSTERDAM",
        "61997" => "CROZET",
        "61998" => "KERGUELEN",
        "67005" => "PAMANDZI",
        "71805" => "ST-PIERRE",
        "78890" => "LA DESIRADE METEO",
        "78894" => "ST-BARTHELEMY METEO",
        "78897" => "LE RAIZET AERO",
        "78922" => "TRINITE-CARAVEL",
        "78925" => "LAMENTIN-AERO",
        "81401" => "SAINT LAURENT",
        "81405" => "CAYENNE-MATOURY",
        "81408" => "SAINT GEORGES",
        "81415" => "MARIPASOULA",
        "89642" => "DUMONT D'URVILLE"
    );

    /**
     * @Route("/meteo/ordered/{station_id}", name="FINALU")
     */
    public function corruption($station_id = 7005)
    {
        $max_temp = 273.15;
        $min_temp;
        $ave_wind;

        $datas = $this->getDoctrine()
        ->getRepository(WeatherData::class)
        ->groupByStation($station_id);

        foreach($datas as &$data){
            $data['ww'] = $this->imageTemps($data['ww']);
            $min_temp = $data['t'];
            $max_temp = max($max_temp, $data['t']);
            $min_temp = min($min_temp, $data['t']);
            $ave_wind[] = $data['ff'];
        }
        $ave_wind = array_sum($ave_wind) / count($ave_wind);

        return $this->render('main/finalu.html.twig', [
            'stations' => $this->stations,
            'datas' => $datas,
            'max'=> $max_temp,
            'min'=> $min_temp,
            'ave'=> $ave_wind
        ]);
    }

    public function imageTemps(?int $temps){
        switch (true) {
            case in_array($temps, range(0,19)):
                return 'picto/sunny.png';
            case in_array($temps, range(20,29)):
                return 'picto/clouds-and-sun.png';
            case in_array($temps, range(30,39)):
                return 'picto/weather-1.png';
            case in_array($temps, range(40,49)):
                return 'picto/morning-snow.png';
            case in_array($temps, range(50,59)):
                return 'picto/tide-1.png';
            case in_array($temps, range(60,69)):
                return 'picto/raining.png';
            case in_array($temps, range(70,79)):
                return 'picto/raining.png';
            case in_array($temps, range(80,94)):
                return 'picto/summer-rain.png';
            case in_array($temps, range(95,99)):
                return 'picto/storm-2.png';
        }
    }

    /**
     * @Route("/meteo/stations", name="main")
     */
    public function index()
    {
        $datas = $this->getDoctrine()
        ->getRepository(WeatherData::class)
        ->orderByAlphabetical();

        return $this->render('main/index.html.twig', [
            'stations' => $this->stations,
            'datas' => $datas
        ]);
    }

    /**
     * @Route("/meteo/menu", name="menu")
     */
    public function menuShow()
    {
        return $this->render('main/menu.html.twig');
    }

    /**
     * @Route("/meteo/stations/rain", name="stationsRain")
     */
    public function byRain()
    {
        $datas = $this->getDoctrine()
        ->getRepository(WeatherData::class)
        ->filterByRain();

        return $this->render('main/filtered.html.twig', [
            'stations' => $this->stations,
            'datas' => $datas
        ]);
    }

    /**
     * @Route("/meteo/stations/{stationId}", name="byId", requirements={"id"="\d+"})
     */
    public function byId($stationId)
    {
        $datas = $this->getDoctrine()
        ->getRepository(WeatherData::class)
        ->findOneBy(['station' => $stationId]);

        if (!$datas) {
            return $this->render('main/error.html.twig',['error'=>'id']);
        }

        return $this->render('main/station.html.twig', [
            'stations' => $this->stations,
            'data' => $datas
        ]);
    }

    /**
     * @Route("/meteo/stations/order/{param}", name="orderByParam")
     */
    public function orderBy($param)
    {
        $datas = null;
        if($param == 'wind_direction') {
            $datas = $this->getDoctrine()
            ->getRepository(WeatherData::class)
            ->filterByWindD();
        }
        else if($param == 'temperature') {
            $datas = $this->getDoctrine()
            ->getRepository(WeatherData::class)
            ->filterByTemp();
        }
        else{
            return $this->render('main/error.html.twig',['error'=>'order']);
        }

        return $this->render('main/filtered.html.twig', [
            'stations' => $this->stations,
            'datas' => $datas
        ]);

    }


}