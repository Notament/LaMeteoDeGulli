<?php

namespace App\Repository;

use App\Entity\WeatherData;
use App\Entity\Station;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Data|null find($id, $lockMode = null, $lockVersion = null)
 * @method Data|null findOneBy(array $criteria, array $orderBy = null)
 * @method Data[]    findAll()
 * @method Data[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeatherDataRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, WeatherData::class);
    }

   /**
    * @return WeatherData[] Returns an array of Data objects
    */

    public function filterByTemp()
    {
        return $this->createQueryBuilder('d')
        ->OrderBy('d.t','ASC')
        ->getQuery()
        ->getResult();
    }

    public function filterByWindD()
    {
        return $this->createQueryBuilder('d')
        ->OrderBy('d.dd','ASC')
        ->getQuery()
        ->getResult();
    }

    public function filterByRain(){
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery('
        SELECT d
        FROM App\Entity\WeatherData d
        WHERE d.ww
        BETWEEN 50 And 69
        ');

        return $query->execute();
    }

    public function orderByAlphabetical(){

        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT d.* FROM weather_data as d
            INNER JOIN station as s
            ON s.numer_sta = d.numer_sta
            ORDER BY s.name ASC
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute();


        return $stmt->fetchAll();
    }

    public function groupByStation($id_station){

        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT d.* FROM weather_data as d
            WHERE d.numer_sta = :station
            ORDER BY d.date ASC
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['station' => $id_station]);


        return $stmt->fetchAll();
    }


}