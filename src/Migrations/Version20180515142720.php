<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180515142720 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE weather_data ADD numer_sta_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE weather_data ADD CONSTRAINT FK_3370691AA7B87674 FOREIGN KEY (numer_sta_id) REFERENCES station (id)');
        $this->addSql('CREATE INDEX IDX_3370691AA7B87674 ON weather_data (numer_sta_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE weather_data DROP FOREIGN KEY FK_3370691AA7B87674');
        $this->addSql('DROP INDEX IDX_3370691AA7B87674 ON weather_data');
        $this->addSql('ALTER TABLE weather_data DROP numer_sta_id');
    }
}
